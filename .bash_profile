##
# File location for .bash_profile
# ~/.bash_profile
##

##
# These are my WP-CLI & MAMP specific configs
# This is in addition to my systems default configs
##

export PATH="/Users/$USER/Scripts:$PATH"
PHP_VERSION=$(ls /Applications/MAMP/bin/php/ | sort -n | tail -1)
export PATH="/Applications/MAMP/bin/php/${PHP_VERSION}/bin:$PATH"
export PATH="$PATH:/Applications/MAMP/Library/bin/"
alias wp-install="~/Scripts/wp-install.sh"
alias wp-delete="~/Scripts/wp-delete.sh"