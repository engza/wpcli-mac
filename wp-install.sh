#! /bin/bash

##
# File location for wp-install.sh
# ~/Scripts/wp-install.sh
##

# Includes your config file
source config.sh

# Log File Location
LOGFILE=~/Scripts/wp-cli.log

printf "\n"

# Check if this is a WordPress project
read -r -p "Is this a WordPress website? <y/n> " response
response=$(echo $response | awk '{print tolower($0)}')
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then

  # Make sure you're in the right directory
  cd /Applications/MAMP/htdocs/

  # List subdirectories to ~/htdocs
  printf "\nParent Directory Options:\n\n"
  ls -l $SITE_PATH | egrep '^d' | awk '{print $9}'

  # Ask which directory website should be created in
  printf "\nParent Directory for installation: "
  read -e SITEDIR
  SITEDIR=${SITEDIR%/}
  printf "\nCREATE - $(date "+%Y%m%d %T") - Success: Parent directory defined '$SITEDIR'.\n" >> $LOGFILE 2>&1

  # Ask for the website name
  printf "\nWebsite Name: "
  read -e TITLE

  # Lowercase and hyphenate for file name
  ROOTDIR=$(echo $TITLE | awk '{print tolower($0)}')
  ROOTDIR=$(echo $ROOTDIR | sed -e 's/ /-/g')

  # Check if directory already exists
  if [ -d $SITE_PATH/$SITEDIR/$ROOTDIR ]
  then
    printf "\nError: '$TITLE' already exists.\n"
    printf "\nAborting.\n\n"
    printf "CREATE - $(date "+%Y%m%d %T") - Error:   '$TITLE' already exists.\n" >> $LOGFILE 2>&1
    exit 1
  else
    printf "CREATE - $(date "+%Y%m%d %T") - Success: Title defined '$TITLE'\n" >> $LOGFILE 2>&1
  fi

  # Create the database.
  DB_NAME=wordpress_$(echo $ROOTDIR | sed -e 's/-//g')
  printf "\nCreating database '$DB_NAME'.\n\n"

  mysql -u$DB_USER -p$DB_PASS -e"CREATE DATABASE $DB_NAME"

  # Check if the database was created successfully
  if [ -d $DB_LOCAL_LOCATION/$DB_NAME ] ; then
    printf "\nSuccess: Created database '$DB_NAME'.\n\n"
    echo "CREATE - $(date "+%Y%m%d %T") - Success: Database '$DB_NAME' created" >> $LOGFILE 2>&1
  else
    printf "\nError: There was an issue creating the database.\n\n"
    echo "CREATE - $(date "+%Y%m%d %T") - Error:   There was an issue creating the '$DB_NAME' database" >> $LOGFILE 2>&1
  fi

  # Download WP Core.
  wp core download --path=$SITE_PATH/$SITEDIR/$ROOTDIR

  FULLDIR=$(echo $SITEDIR/$ROOTDIR | sed -e 's/\/\//\//g')

  printf "\nSuccess: WordPress extracted to '$SITE_PATH$FULLDIR/'.\n\n"
  echo "CREATE - $(date "+%Y%m%d %T") - Success: WordPress extracted to '$SITE_PATH$FULLDIR/'" >> $LOGFILE 2>&1

  # Generate the wp-config.php file
  wp core config --path=$SITE_PATH/$FULLDIR --dbname=$DB_NAME --dbuser=$DB_USER --dbpass=$DB_PASS --extra-php <<PHP
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);
define('WP_MEMORY_LIMIT', '256M');
PHP
  echo "CREATE - $(date "+%Y%m%d %T") - Success: Variables defined in 'wp-config.php' file" >> $LOGFILE 2>&1

  printf "\n"

  # Install the WordPress database.
  wp core install --path=$SITE_PATH/$FULLDIR --url=$BASE_URL/$FULLDIR --title="$TITLE" --admin_user=$WP_USER --admin_password=$WP_PASS --admin_email=$WP_EMAIL
  echo "CREATE - $(date "+%Y%m%d %T") - Success: Database populated" >> $LOGFILE 2>&1
  echo "CREATE - $(date "+%Y%m%d %T") - Success: Website Title set to '$TITLE'" >> $LOGFILE 2>&1
  echo "CREATE - $(date "+%Y%m%d %T") - Success: Admin Username set to '$WP_USER'" >> $LOGFILE 2>&1
  echo "CREATE - $(date "+%Y%m%d %T") - Success: Admin Password set to '$WP_PASS'" >> $LOGFILE 2>&1
  echo "CREATE - $(date "+%Y%m%d %T") - Success: Admin Email set to '$WP_EMAIL'" >> $LOGFILE 2>&1

  printf "\n"

  # Update Options
  wp option update timezone_string "America/Chicago" --path=$SITE_PATH/$FULLDIR
  printf "\n"
  wp option update blogdescription "" --path=$SITE_PATH/$FULLDIR
  printf "\n"
  wp option update permalink_structure "/blog/%postname%/" --path=$SITE_PATH/$FULLDIR
  printf "\n"
  wp option update show_on_front "page" --path=$SITE_PATH/$FULLDIR

  printf "\n"

  # Themes to delete
  DELETEWPTHEMES=( twentysixteen twentyseventeen )

  wp theme delete ${DELETEWPTHEMES[@]} --path=$SITE_PATH/$FULLDIR
  # Check directory structure for installed themes and output to log
  for DELETEWPTHEME in ${DELETEWPTHEMES[@]}; do
    if [ -d $SITE_PATH/$FULLDIR/wp-content/themes/$DELETEWPTHEME ]
    then
      echo "CREATE - $(date "+%Y%m%d %T") - Error:   Theme '$DELETEWPTHEME' not deleted." >> $LOGFILE 2>&1
    else
      echo "CREATE - $(date "+%Y%m%d %T") - Success: Deleted '$DELETEWPTHEME' theme." >> $LOGFILE 2>&1
    fi
  done

  printf "\n"

  # Remove default plugins, install plugins, install Base Theme
  wp plugin delete --path=$SITE_PATH/$FULLDIR --all
  DELETEWPPLUGINS=( akismet hello)
  # Check directory structure for installed plugins and output to log
  for DELETEWPPLUGIN in ${DELETEWPPLUGINS[@]}; do
    # change hyphens (-) into spaces ( )
    DELETEWPPLUGINTITLE=$(echo $DELETEWPPLUGIN | sed -e 's/-/ /g')
    # make the first letter of each word capitalized
    DELETEWPPLUGINTITLE=$(echo $DELETEWPPLUGINTITLE | awk '{for(i=1;i<=NF;i++)sub(/./,toupper(substr($i,1,1)),$i)}1')
    if [ -d $SITE_PATH/$FULLDIR/wp-content/plugins/$DELETEWPPLUGIN ] || [ -e $SITE_PATH/$FULLDIR/wp-content/plugins/$DELETEWPPLUGIN.php ]
    then
      echo "CREATE - $(date "+%Y%m%d %T") - Error:   Plugin '$DELETEWPPLUGINTITLE' not deleted." >> $LOGFILE 2>&1
    else
      echo "CREATE - $(date "+%Y%m%d %T") - Success: Deleted '$DELETEWPPLUGINTITLE' plugin." >> $LOGFILE 2>&1
    fi
  done

  printf "\n"

  # Plugins to install and activate (slugs)
  WPPLUGINS=( all-in-one-wp-migration easy-theme-and-plugin-upgrades redirection )

  printf "Preparing to install the following plugins:\n"

  for WPPLUGIN in ${WPPLUGINS[@]}; do
    WPPLUGINTITLE=$(echo $WPPLUGIN | sed -e 's/-/ /g') # change hyphens (-) into spaces ( )
    WPPLUGINTITLE=$(echo $WPPLUGINTITLE | awk '{for(i=1;i<=NF;i++)sub(/./,toupper(substr($i,1,1)),$i)}1') # make the first letter of each word capitalized
    printf "$WPPLUGINTITLE\n"
  done

  printf "\n"

  wp plugin install ${WPPLUGINS[@]} --path=$SITE_PATH/$FULLDIR --activate

  # Check directory structure for installed plugins and output to log
  for WPPLUGIN in ${WPPLUGINS[@]}; do
    # change hyphens (-) into spaces ( )
    WPPLUGINTITLE=$(echo $WPPLUGIN | sed -e 's/-/ /g')
    # make the first letter of each word capitalized
    WPPLUGINTITLE=$(echo $WPPLUGINTITLE | awk '{for(i=1;i<=NF;i++)sub(/./,toupper(substr($i,1,1)),$i)}1')
    if [ -d $SITE_PATH/$FULLDIR/wp-content/plugins/$WPPLUGIN ]; then
      echo "CREATE - $(date "+%Y%m%d %T") - Success: Plugin '$WPPLUGINTITLE' installed." >> $LOGFILE 2>&1
    else
      echo "CREATE - $(date "+%Y%m%d %T") - Error:   Plugin '$WPPLUGINTITLE' not installed." >> $LOGFILE 2>&1
    fi
  done

  printf "\n"


  printf "Generating 4 posts with dummy content.\n"
  curl http://loripsum.net/api/4 | wp post generate --path=$SITE_PATH/$FULLDIR --post_author=$WP_USER --post_content --count=4
  printf "Success: 4 posts created\n"
  echo "CREATE - $(date "+%Y%m%d %T") - Success: 4 posts with dummy content generated" >> $LOGFILE 2>&1

  # Check if in MAMP directory
  printf "\n"
  read -r -p "Do you want to create a custom plugin? <y/n> " pluginresponse
  pluginresponse=$(echo $pluginresponse | awk '{print tolower($0)}')
  if [[ $pluginresponse =~ ^(yes|y| ) ]] || [[ -z $pluginresponse ]]; then
    printf "\nCreate a plugin template\n"
    wp scaffold plugin --path=$SITE_PATH/$FULLDIR --prompt
    echo "CREATE - $(date "+%Y%m%d %T") - Success: Custom plugin template generated" >> $LOGFILE 2>&1
  else
    echo "CREATE - $(date "+%Y%m%d %T") - Success: User opted not to generate a custom plugin template" >> $LOGFILE 2>&1
  fi

  printf "\nYour new WordPress website is available:\n"
  printf "http://localhost/$FULLDIR/\n"
  printf "http://localhost/$FULLDIR/wp-login.php\n"
  printf "\nCongratulations!\n\n"
  echo "CREATE - $(date "+%Y%m%d %T") - Success: Wordpress website created - http://localhost/$FULLDIR/" >> $LOGFILE 2>&1

else

  printf "\nCurrently not supported.\n\n"
  printf "Aborting.\n\n"
  exit 1

fi