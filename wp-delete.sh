#! /bin/bash

##
# File location for wp-delete.sh
# ~/Scripts/wp-delete.sh
##

# Include the config file
source config.sh

# Log File Location
LOGFILE=~/Scripts/wp-cli.log

printf "\n"

# Check if in MAMP directory
read -r -p "Is this a MAMP website? <y/n> " response
response=$(echo $response | awk '{print tolower($0)}')
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then

  # Make sure you're in the right directory
  cd /Applications/MAMP/htdocs/

  # List subdirectories to ~/htdocs
  printf "\nParent Directory Options:\n\n"
  ls -l $SITE_PATH | egrep '^d' | awk '{print $9}'

  # Ask which directory website should be deleted from
  printf "\nParent Directory for site: "
  read -e SITEDIR
  # Remove trailing slash
  SITEDIR=${SITEDIR%/}
  printf "\nDELETE - $(date "+%Y%m%d %T") - Parent directory defined: $SITEDIR\n" >> $LOGFILE 2>&1

  # Make sure you're in the right directory
  cd /Applications/MAMP/htdocs/$SITEDIR

  # Ask for the website name
  printf "\nDirectory name of website (.../$SITEDIR/?): "
  read -e ROOTDIR
  # Remove trailing slash
  ROOTDIR=${ROOTDIR%/}
  echo "DELETE - $(date "+%Y%m%d %T") - Root directory defined: $ROOTDIR" >> $LOGFILE 2>&1

  # Check if director entered is valid
  if [ ! -d $SITE_PATH/$SITEDIR/$ROOTDIR ]; then
    printf "\nError: Directory '$SITE_PATH/$SITEDIR/$ROOTDIR/' does not exist.\n\nAborting.\n\n"
    echo "DELETE - $(date "+%Y%m%d %T") - Error: directory '$SITE_PATH/$SITEDIR/$ROOTDIR/' does not exist." >> $LOGFILE 2>&1
    echo "DELETE - $(date "+%Y%m%d %T") - Aborting." >> $LOGFILE 2>&1
    exit 1
  fi

  # change hyphens (-) into spaces ( )
  TITLE=$(echo $ROOTDIR | sed -e 's/-/ /g')
  # make the first letter of each word capitalized
  TITLE=$(echo $TITLE | awk '{for(i=1;i<=NF;i++)sub(/./,toupper(substr($i,1,1)),$i)}1')

  printf "\n"

  # Are you sure?
  read -r -p "Are you sure you want to delete the files and DB for '$TITLE'? <y/n> " REPLY
  REPLY=$(echo $response | awk '{print tolower($0)}')
  if [[ $REPLY =~ ^(yes|y| ) ]] || [[ -z $REPLY ]]; then

    echo "DELETE - $(date "+%Y%m%d %T") - Delete process for '$TITLE' confirmed" >> $LOGFILE 2>&1

    printf '\nDeleting files.\n'

    DEST=$SITEDIR/$ROOTDIR

    # Delete files
    rm -rf $SITE_PATH/$DEST/

    if [ ! -d $SITE_PATH/$DEST/ ]; then
      printf '\nSuccess: Files deleted.\n'
      echo "DELETE - $(date "+%Y%m%d %T") - Success: directory '$SITE_PATH/$SITEDIR/$ROOTDIR/' deleted" >> $LOGFILE 2>&1
    else
      printf '\nError: Directory still remains.\n'
      echo "DELETE - $(date "+%Y%m%d %T") - Error: Directory '$SITE_PATH/$SITEDIR/$ROOTDIR/' still remains" >> $LOGFILE 2>&1
    fi

    # Delete the database.
    DB_NAME=wordpress_$(echo $ROOTDIR | sed -e 's/-//g')
    printf "\nDeleting database '$DB_NAME'.\n"

    mysql -u$DB_USER -p$DB_PASS -e"DROP DATABASE $DB_NAME"

    # Check if the database was deleted successfully
    if [ -d $DB_LOCAL_LOCATION/$DB_NAME ] ; then
      printf "\nError: Database '$DB_NAME' still remains.\n"
      echo "DELETE - $(date "+%Y%m%d %T") - Error: Database '$DB_NAME' still remains" >> $LOGFILE 2>&1
    else
      printf "\nSuccess: Database deleted.\n"
      echo "DELETE - $(date "+%Y%m%d %T") - Success: Database '$DB_NAME' deleted" >> $LOGFILE 2>&1
    fi

    printf '\nSuccess: WordPress website deletion complete.\n\n'
    echo "DELETE - $(date "+%Y%m%d %T") - Success: WordPress website deletion complete" >> $LOGFILE 2>&1

  fi
else
  printf "\nCurrently not supported.\n\n"
  printf "Aborting.\n\n"
fi