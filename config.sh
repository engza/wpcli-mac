#! /bin/bash

##
# File location for config.sh
# ~/Scripts/config.sh
##

# Path to your custom themes/plugins
PROJECT_PATH="/Applications/MAMP/htdocs/"

# Path to your WordPress installs
SITE_PATH="/Applications/MAMP/htdocs/"

# Path to your custom Scripts folder
SCRIPT_PATH="/Users/$USER/Scripts"

# Base URL (sites will be setup as subdirectories)
BASE_URL="http://localhost"

# Database information
DB_USER="root"
DB_PASS="root"
DB_HOST="localhost"
DB_LOCAL_LOCATION="/Applications/MAMP/db/mysql57/"

# WordPress Admin Information
WP_USER="admin"
WP_PASS="password"
WP_EMAIL="example@domain.com"